<?php
# Get ACF
# slider options
$type = get_field('my_slider_type');
$speed = get_field('param')['speed'];
$anim_speed = get_field('param')['anim_speed'];

# video
$videomp4 = get_field('url')['slider_video_mp4'];
$videowebm = get_field('url')['slider_video_webm'];
$videoposter = get_field('url')['slider_video_poster'];

# images
$isDiapo = get_field('isSingle');
$images = get_field('images_diapo');
$image_single = get_field('image_single');

# title
$is_single_title = !get_field('single_title');
if($is_single_title){
  $title = get_field('title')['text'];
  $title_css = get_field('title')['css'];
  $title_el = get_field('title')['balise'];
  $title_attr = get_field('title')['attr'];
}else{
  $titles = get_field('multiple_titles')['titles'];
  $title_el = get_field('multiple_titles')['balise'];
  $title_css = get_field('multiple_titles')['css'];
}

# subtitle
$is_single_subtitle = !get_field('single_subtitle');
if($is_single_subtitle){
  $subtitle = get_field('subtitle')['text'];
  $subtitle_css = get_field('subtitle')['css'];
  $subtitle_el = get_field('subtitle')['balise'];
  $subtitle_attr = get_field('subtitle')['attr'];
}else{
  $subtitles = get_field('multiple_subtitles')['subtitles'];
  $subtitle_el = get_field('multiple_subtitles')['balise'];
  $subtitle_css = get_field('multiple_subtitles')['css'];
}

# short text
$short = get_field('short');

?>

<!-- SLIDER BEGIN -->
<section <?php if(isset($block['anchor'])) echo 'id="'.$block['anchor'].'"'; ?> class="slider appear-section <?php if(isset($block['className'])) echo $block['className']; ?>">
    <?php if(!my_wp_is_mobile()): ?>
      <?php   if(!$type): ?>

        <?php if(!$isDiapo): ?>
            <img src="<?php echo $image_single['sizes']['medium'] ?>" alt="<?php echo $image_single['alt'] ?>"/>
        <?php else:
          if($images && !my_wp_is_mobile()): ?>

          <div class="slider-container">

            <div class="slider-prev-arrow">
                <svg class="appear" width="100%" height="100%" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <title>Arrow</title>
                    <g id="Welcome" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="Group-19" transform="translate(0.000000, -0.000000)" fill="#FFFFFF" fill-rule="nonzero">
                            <g id="arrow-down" transform="translate(6.000000, 3.000000)">
                                <polygon id="Stroke-1" transform="translate(5.500000, 8.389086) rotate(-90.000000) translate(-5.500000, -8.389086) " points="5.5 10.5378859 12.2704628 3.3160589 13.7295372 4.6839411 5.5 13.4621141 -2.7295372 4.6839411 -1.2704628 3.3160589"></polygon>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>

            <div class="main-slider" speed="<?php echo $speed ?>" anim-speed="<?php echo $anim_speed ?>">

          <?php foreach ($images as $single): ?>
              <img class="main-slider-bkg" data-skip-lazy="" src="" data-lazy="<?php echo esc_url($single['url']) ?>" alt="<?php echo $single['alt'] ?>">
          <?php  endforeach; ?>


            </div>

            <div class="slider-next-arrow">
                <svg class="appear" width="100%" height="100%" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <title>Arrow</title>
                    <g id="Welcome" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="Group-19" transform="translate(0.000000, -0.000000)" fill="#FFFFFF" fill-rule="nonzero">
                            <g id="arrow-down" transform="translate(6.000000, 3.000000)">
                                <polygon id="Stroke-1" transform="translate(5.500000, 8.389086) rotate(-90.000000) translate(-5.500000, -8.389086) " points="5.5 10.5378859 12.2704628 3.3160589 13.7295372 4.6839411 5.5 13.4621141 -2.7295372 4.6839411 -1.2704628 3.3160589"></polygon>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>

            <div id="slider-dots">

            </div>

          </div>
        <?php else:?>
          <img src="<?php echo $images[0]['sizes']['medium']; ?>" alt="<?php echo $images[0]['alt'] ?>"/>

        <?php endif;
        endif; ?>
          <div class="greyscale"></div>
      <?php else: ?>
        <video playsinline="" autoplay="" loop="" muted="" src="<?php echo $videomp4; ?>" poster="<?php echo $videoposter['url']; ?>">
          <source src="<?php echo $videowebm; ?>"
                  type="video/webm">
          <source src="<?php echo $videomp4; ?>"
                  type="video/mp4">
        </video>
      <?php endif; ?>

    <?php else : ?>
        <img class="poster" src="<?= $videoposter['sizes']['large'] ?>" alt="<?= $videoposter['alt'] ?>">
    <?php endif; ?>
    
      <div class="slider-content">
        <div></div>
        <div class="slider-text appear">
        <?php if($is_single_title): #single title?>
              <?php if($title_el): ?>
                <<?php echo $title_el; ?> class="<?php echo $title_css; ?>" <?php echo $title_attr; ?>><?php echo $title; ?></<?php echo $title_el; ?>>
              <?php endif; ?>
        <?php else: #title > 1 ?>
            <?php if(have_rows('multiple_titles')): while(have_rows('multiple_titles')): the_row(); ?>
              <?php if(have_rows('titles')): $i=0; ?>
                <<?php echo $title_el; ?> class="<?php echo $title_css; ?>">
                  <?php while(have_rows('titles')): the_row(); $i++; #foreach titles ?>
                    <?php if($i!=1) echo '<br>'; ?><?php if(get_sub_field('title')['css']) echo '<span class="'. get_sub_field('title')['css'] . '">'; ?><?php echo get_sub_field('title')['text'];; ?><?php if(get_sub_field('title')['css']) echo '</span>'; ?>
                  <?php endwhile; ?>
                </<?php echo $title_el; ?>>
              <?php endif; ?>
            <?php endwhile; endif; ?>
        <?php endif; ?>

        <?php if($is_single_subtitle): #single subtitle?>
              <?php if($subtitle_el): ?>
                <<?php echo $subtitle_el; ?> class="<?php echo $subtitle_css; ?>" <?php echo $subtitle_attr; ?>><?php echo $subtitle; ?></<?php echo $subtitle_el; ?>>
              <?php endif; ?>
        <?php else: #subtitle > 1 ?>
        <?php if(have_rows('multiple_subtitles')): while(have_rows('multiple_subtitles')): the_row(); ?>
              <?php if(have_rows('subtitles')): $i=0; ?>
                <<?php echo $subtitle_el; ?> class="<?php echo $subtitle_css; ?>">
                  <?php while(have_rows('subtitles')): the_row(); $i++; #foreach subtitles ?>
                    <?php if($i!=1) echo '<br>'; ?><?php if(get_sub_field('subtitle')['css']) echo '<span class="'. get_sub_field('subtitle')['css'] . '">'; ?><?php echo get_sub_field('subtitle')['text'];; ?><?php if(get_sub_field('subtitle')['css']) echo '</span>'; ?>
                  <?php endwhile; ?>
                </<?php echo $subtitle_el; ?>>
              <?php endif; ?>
            <?php endwhile; endif; ?>
        <?php endif; ?>

        <?php if(have_rows('btn')):
                while(have_rows('btn')): the_row();
                    $link = get_sub_field('link');
                    $css = get_sub_field('css_class'); ?>
                    <a href="<?php echo $link['url']; ?>" class="<?php echo $css; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
                <?php endwhile;
            endif; ?>
        </div>

        <?php if(!my_wp_is_mobile()): ?>
          <div class="arrows appear">
            <svg width="20" height="11" xmlns="http://www.w3.org/2000/svg">
            <path d="M10.00000014 7.41421367l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781zm0 0l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781z" fill="#FFF" fill-rule="nonzero" />
            </svg>
            <svg width="20" height="11" xmlns="http://www.w3.org/2000/svg">
              <path d="M10.00000014 7.41421367l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781zm0 0l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781z" fill="#FFF" fill-rule="nonzero" />
            </svg>
            <svg width="20" height="11" xmlns="http://www.w3.org/2000/svg">
              <path d="M10.00000014 7.41421367l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781zm0 0l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781z" fill="#FFF" fill-rule="nonzero" />
            </svg>
            <svg width="20" height="11" xmlns="http://www.w3.org/2000/svg">
              <path d="M10.00000014 7.41421367l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781zm0 0l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781z" fill="#FFF" fill-rule="nonzero" />
            </svg>
          </div>
        </div> 
      <?php endif; ?>
  
      </div>
    <div class="trigger-scroll-debug"></div>
    <?php wp_reset_query(); ?>


</section>
<!-- END SLIDER -->
