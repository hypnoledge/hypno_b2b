("use strict");
(function ($, root, undefined) {
  $(function () {
    $(document).ready(function () {

        

        // COLOR LOGO IF HAS SLIDER
        $('.slider').each(function (){
          var that = $(this);
          let st = ScrollTrigger.create({
            trigger: that,
            start: "top top",
            end: "bottom top",
            //markers: true,
            onEnter: () => $('.main-logo').addClass('white-fill'),
            onLeave: () => $('.main-logo').removeClass('white-fill'),
            onEnterBack: () => $('.main-logo').addClass('white-fill'),
            onLeaveBack: () => $('.main-logo').removeClass('white-fill'),
          });
        });


         /**
         *  MAIN SLIDER (Block - Slider)
         *   */
        // IF IS SLIDER TYPE
        $('.main-slider').each(function (){
            let speed = $(this).attr('speed');
            let animSpeed = $(this).attr('anim-speed');
  
            $(this).slick({
              lazyLoad: 'progressive',
              autoplay: true,
              autoplaySpeed: speed,
              speed: animSpeed,
              dots: true,
              appendDots:$('#slider-dots'),
              dotsClass:'slider-dots',
              arrows: true,
              prevArrow: $('.slider-prev-arrow'),
              nextArrow: $('.slider-next-arrow'),
              draggable: false,
              fade: true,
              focusOnSelect: true,
              zIndex: 2,
              responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    arrows: false
                  }
                }
              ]
            });


      });
      

    });
});
})(jQuery, this);

  