<?php

/**
 * Image banner block template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'repeater-2-columns-' . $block['id'];

if (!empty($block['anchor'])) {
    $id = $block['anchor'];
} ?>

<section class="repeater-2-columns <?php echo array_key_exists('className', $block) ? $block['className'] : ''; ?>">
    <div class="container-fluid" style="background: url(<?php the_field('bkg'); ?>);">
        <div class="container">
            <div class="row introduction-row">   
                <div class="col-auto">
                    <h2><?php the_field('title'); ?></h2>
                    <div>
                        <?php the_field('content_intro'); ?>
                    </div>
                </div>
            </div>
            <?php if ( have_rows('repeater_columns') ) : ?>
                <?php while( have_rows('repeater_columns') ) : the_row(); ?>
                
                    <div class="row align-items-center row-repeater justify-content-between">
                        <div class="col-12 col-md-6 block-img">
                            <?php // include(get_template_directory() . "/template-parts/blocks/svg-anim-img/svg-anim-img.php"); ?>
                            <?php if ( get_sub_field('img') ) : $image = get_sub_field('img'); ?>
                            
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                            
                            <?php endif; ?>
                        </div>
                        <div class="col-12 col-md-6 col-xl-5 block-content">
                            <div>
                                <?php the_sub_field('content'); ?>
                            </div>
                            <div>
                                <?php 
                                if( get_sub_field('modal_type') == 'modal_demo' ) {
                                ?>      
                                    <button id="one" class="btn-hypno button-modal btn-demo">
                                        Demander une démo
                                    </button>
                                <?php
                                } elseif ( get_sub_field('modal_type') == 'modal_contact' ) {
                                ?>
                                    <button id="one" class="btn-hypno button-modal-contact btn-demo">
                                        Nous contacter
                                    </button>
                                <?php
                                } else {
                                    if ( get_sub_field('link') ) : $file = get_sub_field('link'); ?>
                                        <a class="btn-hypno" href="<?php echo $file['url']; ?>"><?php echo $file['title']; ?></a>
                                    <?php
                                    endif;
                                }
                                ?> 
                            </div>
                        </div>
                    </div>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>