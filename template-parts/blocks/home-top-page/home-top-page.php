<?php

/**
 * Image banner block template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'home-top-page-' . $block['id'];

if (!empty($block['anchor'])) {
    $id = $block['anchor'];
} ?>

<section class="home-top-page <?php echo array_key_exists('className', $block) ? $block['className'] : ''; ?>" style="background-image: url(<?php the_field('bkg'); ?>);">
    <div class="container h-100">
        <?php
            if( get_field('reverse') ) {
                
                $row_reverse = 'row-reverse';
            }else{
                $row_reverse = '';
            }
            ?>
          
            <div class="row align-items-center h-100 <?php echo $row_reverse; ?>">
                <div class="col-12 col-md-12 col-lg-6 col-content">
                    <div class="block-title">
                        <?php the_field('title'); ?>
                    </div>
                    <div>
                        <?php 

                            if( get_field('modal_type') == 'modal_demo' ) {
                            ?>      
                                <button id="one" class="btn-hypno button-modal btn-demo">
                                    Demander une démo
                                </button>
                              <?php
                            } elseif ( get_field('modal_type') == 'modal_contact' ) {
                            ?>
                                <button id="one" class="btn-hypno button-modal-contact btn-demo">
                                    Nous contacter
                                </button>
                            <?php
                            } else {
                                if ( get_field('link') ) : $file = get_field('link'); ?>
                                    <a class="btn-hypno" href="<?php echo $file['url']; ?>"><?php echo $file['title']; ?></a>
                                <?php
                                endif;
                            }
                        ?>   
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-6 col-bkg ml-auto h-100 col-bkg">
                    <?php include(get_template_directory() . "/template-parts/blocks/svg-anim-img/svg-anim-img.php"); ?>
                    <?php if ( get_field('home_top_bkg') && !wp_is_mobile() ){ ?>
                        <img class="top-home-image-background" src="<?php the_field('home_top_bkg'); ?>" alt="<?php the_field(''); ?>">
                    <?php } ?>
                </div>
            </div>
    </div>
</section>