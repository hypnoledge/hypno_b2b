

<div <?php if(isset($block['anchor'])) echo 'id="'.$block['anchor'].'"'; ?> class="image-svg-anim">

        <div class="anim-image-container position-relative">
            <?php if( have_rows('image_animee') ): ?>
                <?php while( have_rows('image_animee') ): the_row(); 
                    if ( get_sub_field('image') ) : ?>
                        <?php if(get_sub_field('position')){
                            $position = 'absolute';
                        }else{
                            $position = 'relative';
                        }?>
                        <?php 
                        $image = get_sub_field('image');
                        if( !empty( $image ) ): ?>
                            <img class="speed position-<?= $position; ?>" data-speed="<?php the_sub_field('vitesse_scroll');?>" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>

</div>
