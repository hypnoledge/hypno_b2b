<?php

/**
 * Image banner block template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'home-dimensions-' . $block['id'];

if (!empty($block['anchor'])) {
    $id = $block['anchor'];
} ?>
<section class="home-dimensions <?php echo array_key_exists('className', $block) ? $block['className'] : ''; ?>">
    <div class="container-fluid">
        <div class="container">
            <div class="row d-flex flex-column align-items-center justify-content-center d-lg-flex flex-lg-row align-items-lg-start justify-content-lg-between text-center text-lg-left mb-5 mb-lg-3">
                <div class="col-12 col-lg-8 col-xl-6 block-content">
                    <?php the_field('title'); ?>
                </div>
                <div class="col-auto">
                    <?php 
                        if( get_field('modal_type') == 'modal_demo' ) {
                        ?>      
                            <button id="one" class="btn-hypno button-modal btn-demo">
                                Demander une démo
                            </button>
                        <?php
                        } elseif ( get_field('modal_type') == 'modal_contact' ) {
                        ?>
                            <button id="one" class="btn-hypno button-modal-contact btn-demo">
                                Nous contacter
                            </button>
                        <?php
                        } else {
                            if ( get_field('link') ) : $file = get_field('link'); ?>
                                <a class="btn-hypno" href="<?php echo $file['url']; ?>"><?php echo $file['title']; ?></a>
                            <?php
                            endif;
                        }
                    ?> 
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid horizontal-scroll-768">
        <div class="container horizontal-scroll-768">
            <div class="row row-repeater">
                <?php if ( have_rows('blocks_dimensions') ) : ?>
                    <?php while( have_rows('blocks_dimensions') ) : the_row(); ?>

                        <div class="col-12 col-sm-6 col-md-6 col-lg-5 col-xl-3 d-flex flex-column">
                            <div class="d-flex flex-column col-repeater">
                                <div>
                                    <?php if ( get_sub_field('img') ) : $image = get_sub_field('img'); ?>
                                    
                                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                                    
                                    <?php endif; ?>
                                </div>
                                <div class="col-repeater-content">
                                    <h3>
                                        <?php the_sub_field('title'); ?>
                                    </h3>
                                    <div>
                                        <?php the_sub_field('content'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>