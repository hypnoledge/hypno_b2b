<?php

/**
 * Image banner block template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'block-2-columns-full-width-' . $block['id'];

if (!empty($block['anchor'])) {
    $id = $block['anchor'];
} ?>

<section class="block-2-columns-full-width <?php echo array_key_exists('className', $block) ? $block['className'] : ''; ?>">
    <div class="container-fluid" style="background-color: <?php the_field('bkg_color'); ?>;">
        <div class="row">
            <div class="col-12 col-sm-8 col-md-7 block-content">
                <?php if ( get_field('logo') ) : $image = get_field('logo'); ?>
                    <img class="logo-before" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                <?php endif; ?>
                <?php 
                    if( get_field('type_page') ) { ?>
                        <span class="title-tag"><?php the_title(); ?></span>
                    <?php 
                    }
                ?>
                <div>
                    <?php the_field('content'); ?>
                </div>
                <div>
                <?php 
                if( get_field('modal_type') == 'modal_demo' ) {
                ?>      
                    <button id="one" class="btn-hypno-white button-modal btn-demo">
                        Demander une démo
                    </button>
                <?php
                } elseif ( get_field('modal_type') == 'modal_contact' ) {
                ?>
                    <button id="one" class="btn-hypno-white button-modal-contact btn-demo">
                        Nous contacter
                    </button>
                <?php
                } else {
                    if ( get_field('link') ) : $file = get_field('link'); ?>
                        <a class="btn-hypno-white" href="<?php echo $file['url']; ?>"><?php echo $file['title']; ?></a>
                    <?php
                    endif;
                }
                ?>   
                    
                </div>
            </div>
            <div class="col-12 col-sm-4 col-md-5 block-img">
                    <?php if ( get_field('img') ) : $image = get_field('img'); ?>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                    <?php endif; ?>
                    <div class="block-blockquote">
                        <?php the_field('blockquote'); ?>
                    </div>
            </div>
        </div>
    </div>
</section>