<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hypno-b2b
 */

?>
<!-- TEST MODAL -->

<div id="modal-container">
  <div class="modal-background">
    <div class="modal">
		<div class="close-modal">
			<span>Fermer</span>
		</div>
		<h2>Demande d'informations</h2>
		<div>
			<?php the_field('contact_form', 'option'); ?>
		</div>
    </div>
  </div>
</div>

<!-- TEST MODAL -->

	<footer id="colophon" class="site-footer">
		<div class="container-fluid prefooter">
			<div class="container">
				<div class="row justify-content-between">
					<div class="col-auto">
					<?php if( have_rows('logo2', 'option') ): ?>
						<?php while( have_rows('logo2', 'option') ): the_row(); 

							// Get sub field values.
							$image = get_sub_field('light', 'option');

							?>
							<img src="<?php echo esc_url( $image['url'] ); ?>" alt="<?php echo esc_attr( $image['alt'] ); ?>" />

						<?php endwhile; ?>
					<?php endif; ?>
						
					</div>
					<div class="col-auto footer-start-learning">
						<button class="btn-hypno-white">
							<?php if ( get_field('lien_prefooter', 'option') ) : $file = get_field('lien_prefooter', 'option'); ?>
								<a href="<?php echo $file['url']; ?>"><?php echo $file['title']; ?></a>
							<?php endif; ?>
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid main-footer">
			<div class="container">
				<div class="row justify-content-between">
					<?php if ( have_rows('group_links_footer', 'option') ) : ?>
						<?php while( have_rows('group_links_footer', 'option') ) : the_row(); ?>
							<div class="<?php the_sub_field('class_cols', 'option'); ?>">
								<?php the_sub_field('content', 'option'); ?>
							</div>
							
					<?php endwhile; ?>
					<?php endif; ?>
					<div class="footer-fourth-col col-lg-3 col-12">
						<div>
							<h3><?php _e('Nos partenaires'); ?></h3>
							<div class="d-flex footer-partners justify-content-between">
								<?php if ( have_rows('icones_pre_footer', 'option') ) : ?>
									<?php while( have_rows('icones_pre_footer', 'option') ) : the_row(); ?>
									
										<?php if ( get_sub_field('link', 'option') ) : $file = get_sub_field('link', 'option'); ?>
											<a href="<?php echo $file['url']; ?>">
												<?php if ( get_sub_field('image', 'option') ) : $image = get_sub_field('image', 'option'); ?>
												
													<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
												
												<?php endif; ?>
											</a>
										<?php endif; ?>
										
									<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
						<div class="footer-social">
							<h3><?php _e('Rejoindre les Hypnoledgers'); ?></h3>
							<div class="d-flex">
								<!-- FOOTER SOCIAL NETWORK -->
								<?php if (have_rows('option_social_networks', 'option')): ?>
									<?php while (have_rows('option_social_networks', 'option')): the_row(); ?>
										<a class="mr-20" target="_blank" href="<?php the_sub_field('url_network', 'option'); ?>" title="<?php the_sub_field('url_network', 'option'); ?>">
											<img src="<?php echo esc_url(get_sub_field('icon_network', 'option')['url']); ?>" alt="<?php echo esc_attr(get_sub_field('option_social_networks_link', 'option')['title']); ?>">
										</a>
									<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid subfooter">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<p class="text-white text-center mb-0">
							<?php _e('Copyright © 2018 - 2021 Hypnoledge. Tous droits réservés.'); ?>
						</p>
					</div>
				</div>
			</div>		
		</div>
	</footer><!-- #colophon -->

	<?php wp_footer(); ?>

	</body>
</html>
