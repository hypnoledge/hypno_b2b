<?php 

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

         // Quotes and photo
         acf_register_block_type(array(
            'name'              => 'quote-with-photo',
            'title'             => __('Citation avec photo décalée'),
            'description'       => __('Citation avec photo décalée'),
            'category'          => 'custom-blocks',
            'render_template'   => '/template-parts/blocks/quote-with-photo/quote-with-photo.php',
            'supports'          => array( 'anchor' => true),
            // 'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/svg-anim-img/svg-anim-img.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/quote-with-photo/quote-with-photo.css',
        ));
         // Block quote
         acf_register_block_type(array(
            'name'              => 'block-quote',
            'title'             => __('Citation avec crochets'),
            'description'       => __('Citation avec crochets'),
            'category'          => 'custom-blocks',
            'render_template'   => '/template-parts/blocks/block-quote/block-quote.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/block-quote/block-quote.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/block-quote/block-quote.css',
        ));
         // Repeteur 2 colonnes
         acf_register_block_type(array(
            'name'              => 'repeater-2-columns',
            'title'             => __('Répeteur 2 colonnes reversible'),
            'description'       => __('Répeteur 2 colonnes'),
            'category'          => 'custom-blocks',
            'render_template'   => '/template-parts/blocks/repeater-2-columns/repeater-2-columns.php',
            'supports'          => array( 'anchor' => true),
            // 'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/svg-anim-img/svg-anim-img.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/repeater-2-columns/repeater-2-columns.css',
        ));
         // Presentation multi block
         acf_register_block_type(array(
            'name'              => 'presentation-multi-blocks',
            'title'             => __('Multi block presentation plateforme'),
            'description'       => __('Contenu linguistique'),
            'category'          => 'custom-blocks',
            'render_template'   => '/template-parts/blocks/presentation-multi-blocks/presentation-multi-blocks.php',
            'supports'          => array( 'anchor' => true),
            // 'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/svg-anim-img/svg-anim-img.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/presentation-multi-blocks/presentation-multi-blocks.css',
        ));
         // Block 2 colonnes plein largeur
         acf_register_block_type(array(
            'name'              => 'block-2-columns-full-width',
            'title'             => __('Block 2 colonnes plein largeur'),
            'description'       => __('Block 2 colonnes pleine largeur'),
            'category'          => 'custom-blocks',
            'render_template'   => '/template-parts/blocks/block-2-columns-full-width/block-2-columns-full-width.php',
            'supports'          => array( 'anchor' => true),
            // 'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/svg-anim-img/svg-anim-img.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/block-2-columns-full-width/block-2-columns-full-width.css',
        ));
         // Block 2 colonnes
         acf_register_block_type(array(
            'name'              => 'block-2-columns',
            'title'             => __('Block 2 colonnes'),
            'description'       => __('Block 2 colonnes reversible'),
            'category'          => 'custom-blocks',
            'render_template'   => '/template-parts/blocks/block-2-columns/block-2-columns.php',
            'supports'          => array( 'anchor' => true),
            // 'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/svg-anim-img/svg-anim-img.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/block-2-columns/block-2-columns.css',
        ));
         // Banner ressources
         acf_register_block_type(array(
            'name'              => 'banner-ressources',
            'title'             => __('Bannière blog'),
            'description'       => __('Bannière promotionnelle lien vers article'),
            'category'          => 'custom-blocks',
            'render_template'   => '/template-parts/blocks/banner-ressources/banner-ressources.php',
            'supports'          => array( 'anchor' => true),
            // 'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/svg-anim-img/svg-anim-img.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/banner-ressources/banner-ressources.css',
        ));
         // Home dimensions hypno
         acf_register_block_type(array(
            'name'              => 'home-dimensions-hypno',
            'title'             => __('Block multi col'),
            'description'       => __('Présentation des dimensions hypnose'),
            'category'          => 'custom-blocks',
            'render_template'   => '/template-parts/blocks/home-dimensions-hypno/home-dimensions-hypno.php',
            'supports'          => array( 'anchor' => true),
            // 'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/svg-anim-img/svg-anim-img.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/home-dimensions-hypno/home-dimensions-hypno.css',
        ));
         // Home dashboards
         acf_register_block_type(array(
            'name'              => 'home-dashboard',
            'title'             => __('Mockup plein largeur'),
            'description'       => __('Présentation plteforme via mockup'),
            'category'          => 'custom-blocks',
            'render_template'   => '/template-parts/blocks/home-dashboard/home-dashboard.php',
            'supports'          => array( 'anchor' => true),
            // 'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/svg-anim-img/svg-anim-img.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/home-dashboard/home-dashboard.css',
        ));
         // Home targets
         acf_register_block_type(array(
            'name'              => 'home-targets',
            'title'             => __('Cibles clients'),
            'description'       => __('Blocks prospects'),
            'category'          => 'custom-blocks',
            'render_template'   => '/template-parts/blocks/home-targets/home-targets.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/home-targets/home-targets.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/home-targets/home-targets.css',
        ));
         // Haut de page (pages offres et clients)
         acf_register_block_type(array(
            'name'              => 'presta-top-page',
            'title'             => __('Haut de page'),
            'description'       => __('Presta top page'),
            'category'          => 'custom-blocks',
            'render_template'   => '/template-parts/blocks/presta-top-page/presta-top-page.php',
            'supports'          => array( 'anchor' => true),
            // 'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/svg-anim-img/svg-anim-img.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/presta-top-page/presta-top-page.css',
        ));
         // Haut de page HOME
         acf_register_block_type(array(
            'name'              => 'home-top-page',
            'title'             => __('Accueil haut de page'),
            'description'       => __('Home top page'),
            'category'          => 'custom-blocks',
            'render_template'   => '/template-parts/blocks/home-top-page/home-top-page.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/home-top-page/home-top-page.css',
        ));
         // Espacement
         acf_register_block_type(array(
            'name'              => 'spacing',
            'title'             => __('Espacement'),
            'description'       => __('margin block'),
            'category'          => 'custom-blocks',
            'icon'              => 'editor-break',
            'keywords'          => array( 'margin', 'space', 'vh'),
            'render_template'   => '/template-parts/blocks/spacing/spacing.php',
            'supports'          => array( 'anchor' => true),
        ));
        acf_register_block_type(array(
            'name'              => 'block-agrandissable',
            'title'             => __('block-agrandissable'),
            'description'       => __('block agrandissable'),
            'category'          => 'custom-blocks',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'relatedpost', 'quote' ),
            'render_template'   => 'template-parts/blocks/block-agrandissable/content-block-agrandissable.php',
            'supports'          => array( 'anchor' => true),
        ));
        acf_register_block_type(array(
            'name'              => 'vertical-bar',
            'title'             => __('Vertical Bar'),
            'description'       => __('Vertical Bar'),
            'render_template'   => 'template-parts/blocks/vertical-bar/vertical-bar.php',
            'category'          => 'custom-blocks',
            'icon'              => 'tide',
            'keywords'          => array( 'Vertical bar', 'margin', 'vertical'),
            'supports'          => array( 'anchor' => true),
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/vertical-bar/vertical-bar.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/vertical-bar/vertical-bar.css',
        ));
        acf_register_block_type(array(
            'name'              => 'sticky-slide',
            'title'             => __('Sticky Image slide'),
            'description'       => __('Image qui slide puis ya du text qui arrive a un moment'),
            'category'          => 'custom-blocks',
            'icon'              => 'media-document',
            'keywords'          => array( 'yes i am'),
            'render_template'   => 'template-parts/blocks/sticky-slide/sticky-slide.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style'     => get_template_directory_uri() . '/template-parts/blocks/sticky-slide/sticky-slide.css',
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/sticky-slide/sticky-slide.js',
        ));
        acf_register_block_type(array(
            'name'              => 'slider',
            'title'             => __('Slider'),
            'description'       => __('Slider'),
            'render_template'   => 'template-parts/blocks/slider/slider.php',
            'category'          => 'custom-blocks',
            'icon'              => 'format-gallery',
            'keywords'          => array( 'slider', 'video', 'picture'),
            'supports'          => array( 'anchor' => true),
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/slider/slider.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/slider/slider.css',
        ));
        acf_register_block_type(array(
            'name'              => 'svg-anim-img',
            'title'             => __('svg-anim-img'),
            'description'       => __('svg-anim-img'),
            'render_template'   => 'template-parts/blocks/svg-anim-img/svg-anim-img.php',
            'category'          => 'custom-blocks',
            'icon'              => 'format-gallery',
            'keywords'          => array( 'svg-anim-img', 'video', 'picture'),
            'supports'          => array( 'anchor' => true),
        ));


        add_action('acf/init', 'register_acf_block_types');

    }


?>